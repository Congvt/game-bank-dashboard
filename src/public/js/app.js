'use strict'

$(document).ready(() => {
  // $(window).keydown((event) => {
  //   if (event.keyCode === 13) {
  //     event.preventDefault()
  //     return false
  //   }
  // })

  jQuery(() => {
    jQuery('#allinone_bannerRotator_classic').allinone_bannerRotator({
      skin: 'classic',
      width: 962,
      height: 355,
      thumbsWrapperMarginBottom: 5,
      responsive: true,
      responsiveRelativeToBrowser: false,
      centerPlugin: true
    })
  })

  const $popupFailed = $('#khongthanhcong')
  const $popupProcessing = $('#dangxuly')
  const $popupSuccessful = $('#giaodichthanhcong')
  const $popupNotice = $('#thongbao')
  const $inputAmountBuy = $('#amountBuy')
  const $inputAmountSell = $('#amountSell')
  const $inputExchange = $('#exchange')
  let requestId

  function buyRate(amount) {
    if (amount < 10000000) {
      return amount * 80 / 100
    } else {
      return amount * 81 / 100
    }
  }

  function sellRate(amount) {
    return amount * 85 / 100
  }

  $inputAmountBuy.keyup(() => {
    try {
      $inputExchange.val(buyRate(parseInt($inputAmountBuy.val(), 10)))
    }  catch (e) {
      $inputExchange.val('')
    }
  })

  $inputAmountSell.keyup(() => {
    try {
      $inputExchange.val(sellRate(parseInt($inputAmountSell.val(), 10)))
    }  catch (e) {
      $inputExchange.val('')
    }
  })

  function isPopupOpen() {
    return $popupFailed.hasClass('show') || $popupProcessing.hasClass('show')
      || $popupSuccessful.hasClass('show') || $popupNotice.hasClass('show')
  }

  $popupFailed.on('shown.bs.modal', () => {
    $popupProcessing.modal('hide')
    $popupSuccessful.modal('hide')
    $popupNotice.modal('hide')
  })

  $('#buyFrm').submit((event) => {
    event.preventDefault()
    if (isPopupOpen()) {
      return false
    }
    const username = $('#username').val()
    const amount = $inputAmountBuy.val()
    const bank = $('#bank').val()
    const account = $('#account').val()
    const expired = $('#expired').val()
    $popupProcessing.modal({backdrop: 'static', keyboard: false})
    axios.post('/buy', {
      username,
      amount,
      bank,
      account,
      expired
    }).then((res) => {
      requestId = res.data.id
    }).catch((err) => {
      console.error(err.response.data.message)
      $popupFailed.modal()
    })
  })

  $('#sellFrm').submit((event) => {
    event.preventDefault()
    if (isPopupOpen()) {
      return false
    }
    const username = $('#username').val()
    const amount = $inputAmountSell.val()
    const bank = $('#bank').val()
    const account = $('#account').val()
    const expired = $('#expired').val()
    $popupProcessing.modal({backdrop: 'static', keyboard: false})
    axios.post('/sell', {
      username,
      amount,
      bank,
      account,
      expired
    }).then((res) => {
      requestId = res.data.id
    }).catch((err) => {
      console.error(err.response.data.message)
      $popupFailed.modal()
    })
  })
})
