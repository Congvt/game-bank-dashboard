import { Router } from 'express'
import * as mongodb from 'mongodb'
import app from '../app'
import dashboard from './dashboard'

const router = Router()

function buyRate(amount) {
  if (amount < 10000000) {
    return amount * 80 / 100
  } else {
    return amount * 81 / 100
  }
}

function sellRate(amount) {
  return amount * 85 / 100
}

function saveRequest(request) {
  const mongoClient: mongodb.MongoClient = app.locals.mongoClient
  const requests: mongodb.Collection = mongoClient.db('gamebankvn').collection('requests')
  request.createdAt = +(new Date())
  return requests.insertOne(request)
}

function formatDate(timestamp: number) {
  const date = new Date(timestamp)
  return `${date.getFullYear()}/${date.getMonth()}/${date.getDay()}
   ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
}

function format(request: any) {
  return {
    id: request._id,
    createdAt: formatDate(request.createdAt),
    ...request
  }
}

function getRequest(rid) {
  const mongoClient: mongodb.MongoClient = app.locals.mongoClient
  const requests: mongodb.Collection = mongoClient.db('gamebankvn').collection('requests')
  return requests.findOne({ _id: new mongodb.ObjectID(rid) })
    .then((item) => {
      return format(item)
    })
}

function getAllRequests() {
  const mongoClient: mongodb.MongoClient = app.locals.mongoClient
  const requests: mongodb.Collection = mongoClient.db('gamebankvn').collection('requests')
  return requests.find({}).sort({ status: 1 }).toArray()
    .then((items: [any]) => {
      return items.map((item) => format(item))
    })
}

function getBuyRequests() {
  const mongoClient: mongodb.MongoClient = app.locals.mongoClient
  const requests: mongodb.Collection = mongoClient.db('gamebankvn').collection('requests')
  return requests.find({ type: 0 }).sort({ status: 1 }).toArray()
  .then((items: [any]) => {
      return items.map((item) => format(item))
    })
}

function getSellRequests() {
  const mongoClient: mongodb.MongoClient = app.locals.mongoClient
  const requests: mongodb.Collection = mongoClient.db('gamebankvn').collection('requests')
  return requests.find({ type: 1 }).sort({ status: 1 }).toArray()
    .then((items: [any]) => {
      return items.map((item) => format(item))
    })
}

router.get('/dashboard', (req, res) => {
  Promise.all([getBuyRequests(), getSellRequests()])
    .then(([buy, sell]) => {
      const config = {
        path: 'pages/account-secondary',
        title: 'Tài Khoản phụ',
        buy,
        sell
      }
      res.render(config.path, { config });
    })
    .catch((err: Error) => {
      res.status(500).json({ message: err.message })
    })
})

router.get('/dashboard/request/:rid', (req, res) => {
  getRequest(req.params.rid)
    .then((request) => {
      if (request) {
        res.render('dashboard/request', request)
      } else {
        res.status(400).json({ message: 'Invalid data' })
      }
    })
    .catch((err: Error) => {
      res.status(500).json({ message: err.message })
    })
})

router.get('/request', (req, res) => {
  res.json(app.locals.requests.sort((a, b) => {
    return a.status - b.status
  }))
})

router.get('/request/:rid', (req, res) => {
  getRequest(req.params.rid)
    .then((request) => {
      if (request) {
        delete request._id
        res.json(request)
      } else {
        res.status(400).json({ message: 'Invalid data' })
      }
    })
    .catch((err: Error) => {
      res.status(500).json({ message: err.message })
    })
})

router.post('/buy', (req: any, res: any) => {
  const request = {
    type: 0,
    status: 0,
    ...req.body
  }
  if (request.username && request.amount && request.bank && request.account && request.expired) {
    saveRequest(request)
      .then((data: mongodb.InsertOneWriteOpResult<any>) => {
        res.json({ id: data.insertedId })
      })
      .catch((err: Error) => {
        res.status(500).json({ message: err.message })
      })
  } else {
    res.status(400).json({ message: 'Invalid data' })
  }
})

router.post('/sell', (req: any, res: any) => {
  const request = {
    type: 1,
    status: 0,
    ...req.body
  }
  if (request.username && request.amount && request.bank && request.account) {
    saveRequest(request)
      .then((data: mongodb.InsertOneWriteOpResult<any>) => {
        res.json({ id: data.insertedId })
      })
      .catch((err: Error) => {
        res.status(500).json({ message: err.message })
      })
  } else {
    res.status(400).json({ message: 'Invalid data' })
  }
})

router.get(['/', '/:page'], (req: any, res) => {
  if (!req.params.page) {
    req.params.page = 'exchange'
  }
  if (['buy', 'sell', 'exchange'].indexOf(req.params.page) < 0) {
    res.status(400).send('Not Found!')
  } else {
    let leftBanner
    let rightBanner
    let centerBanner = []
    let title = 'GameBankVN'
    let description = ''
    let fb = ''
    let zalo = ''
    let tel = ''
    const mongoClient: mongodb.MongoClient = app.locals.mongoClient
    const configs: mongodb.Collection = mongoClient.db('gamebankvn').collection('configs')
    configs.find({}).toArray()
      .then((items) => {
        items.forEach((item: { name, value }) => {
          if (item.name === 'leftBanner') {
            leftBanner = item.value
          } else if (item.name === 'rightBanner') {
            rightBanner = item.value
          } else if (item.name === 'centerBanner') {
            centerBanner = item.value
          } else if (item.name === 'title') {
            title = item.value
          } else if (item.name === 'description') {
            description = item.value
          } else if (item.name === 'fb') {
            fb = item.value
          } else if (item.name === 'zalo') {
            zalo = item.value
          } else if (item.name === 'tel') {
            tel = item.value
          }
        })
      })
      .catch((err) => {
        console.error(err)
      })
      .then(() => {
        res.render('index', {
          page: req.params.page,
          title,
          description,
          banners: {
            left: leftBanner,
            right: rightBanner,
            center: centerBanner
          },
          contact: {
            fb,
            zalo,
            tel
          }
        })
      })
  }
})

export default router
