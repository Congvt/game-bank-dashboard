import { Router } from 'express'

const router = Router()

// Authenticate
router.get('/dashboard/login', (req, res) => {
  const config = {
    path: 'pages/login',
    title: 'login'
  }
  res.render(config.path, { config });
});

router.get('/dashboard/change-password', (req, res) => {
  const config = {
    path: 'pages/change-password',
    title: 'change-password'
  }
  res.render(config.path, { config });
});

/* GET home page. */
// router.get('/dashboard', (req, res) => {
//   const config = {
//     path: 'pages/account-secondary',
//     title: 'Tài Khoản phụ'
//   }
//   res.render(config.path, { config });
// });

router.get('/dashboard/buy', (req, res) => {
  const config = {
    path: 'pages/buy',
    title: 'Mua'
  }
  res.render(config.path, { config });
});

router.get('/dashboard/sell', (req, res) => {
  const config = {
    path: 'pages/sell',
    title: 'Bán'
  }
  res.render(config.path, { config });
});

router.get('/dashboard/banner', (req, res) => {
  const config = {
    path: 'pages/banner',
    title: 'Banner'
  }
  res.render(config.path, { config });
});

router.get('/dashboard/contact', (req, res) => {
  const config = {
    path: 'pages/contact',
    title: 'Liên lạc'
  }
  res.render(config.path, { config });
});

export default router
