import { Server } from 'http'
import * as mongodb from 'mongodb'
import app from './app'

let server: Server

const closeServer = () => {
  if (app.locals.mongoClient) {
    app.locals.mongoClient.close(() => {
      console.log('Closed mongo connection!')
    })
  }
  server.close((error: any) => {
    if (error) {
      process.exit(1)
    } else {
      process.exit(0)
    }
  })
}

process.on('unhandledRejection', () => {
  // logger.error(error, 'Uncaught Error')
})

process.on('uncaughtException', () => {
  // logger.error(error, 'Uncaught Error')
})

// exit server when close app
process.on('SIGINT', closeServer)

mongodb.MongoClient.connect('mongodb://localhost:27017',
  {
    auth: {
      user: 'root',
      password: '123456'
    },
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  (err, client: mongodb.MongoClient) => {
    if (err) {
      console.error(err)
    } else {
      app.locals.mongoClient = client
      server = app.listen(3001, (httpErr: any) => {
        if (httpErr) {
          closeServer()
        }
        console.log(`server is listening on 3001`)
      })
    }
  })
