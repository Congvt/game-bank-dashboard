import express, { Response } from 'express'
import ejsLayout from 'express-ejs-layouts'
import path from 'path'
import requestId from 'request-id/express'
import dashboardRouter from './routes/dashboard'
import router from './routes/index'
const app = express()

app.set('trust proxy', true)
app.set('json spaces', 2)

// ------ Toggle dashboard & admin ------ //
// app.set('views', path.join(__dirname, 'views'))
app.set('views', path.join(__dirname, 'views/dashboard'))

app.set('view engine', 'ejs')
app.use(ejsLayout)
app.locals.requests = []

app.use('/public', express.static(path.join(__dirname, 'public')))

app.use(requestId())

app.use(express.json({ limit: '50mb' }))
app.use(express.urlencoded({
  limit: '50mb',
  extended: true,
  parameterLimit: 50000
}))

// ------ Toggle router ------ //
app.use(router)
app.use(dashboardRouter)

// catch 404 error
app.use((req: any, res: Response) => {
  res.status(404).json({
    error: {
      requestId: req.requestId,
      message: `Not found ${req.originalUrl}`
    }
  })
})

// Error handler
app.use((err, req, res: Response, next) => {
  if (res.headersSent) {
    return next(err)
  }
  res.status(500).json({
    error: {
      requestId: req.requestId,
      message: err.message,
      stack: err.stack
    }
  })
})

export default app
